var net = require("net");
var JSONStream = require('JSONStream');
var lane = require('./lane.js');
var car = require("./car.js");

var serverHost = process.argv[2];
var serverPort = process.argv[3];
var botName = process.argv[4];
var botKey = process.argv[5];

console.log("I'm", botName, "and connect to", serverHost + ":" + serverPort);

client = net.connect(serverPort, serverHost, function() {
  return send({
    msgType: "join",
    data: {
      name: botName,
      key: botKey
    }
  });
  
	/*return send({
		msgType: "createRace", 
		data: {
			botId: {
				name: botName,
				key: botKey
			},
			trackName: "germany",
			password: "schumi4ever",
			carCount: 1
	}});*/
});

function send(json) {
  client.write(JSON.stringify(json));
  return client.write('\n');
};

jsonStream = client.pipe(JSONStream.parse());

var laneObj;
var carObj;

jsonStream.on('data', function(data) {
  if (data.msgType === 'carPositions') {
	send(carObj.progress(data.data, laneObj));
  } else {
	console.log(data)
    if (data.msgType === 'join') {
		console.log('Joined')
	} else if (data.msgType === 'yourCar') {
		carObj = new car.Car(data.data);
	} else if (data.msgType === 'gameInit') {
		laneObj = new lane.Lane(data.data.race.track.pieces, data.data.race.track.lanes);
    } else if (data.msgType === 'gameStart') {
		console.log('Race started');
    } else if (data.msgType === 'gameEnd') {
		console.log('Race ended');
		laneObj.printAngles();
    } else if (data.msgType === 'crash') {
		carObj.crash(data.data);
	}

    send({
		msgType: "ping",
		data: {}
    });
  }
});

jsonStream.on('error', function() {
  return console.log("disconnected");
});
