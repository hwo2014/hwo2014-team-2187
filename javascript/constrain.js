
function Constrain(source, dest, distFactor, softDist) {
	this.source = source;
	this.dest = dest;
	if (softDist == 0) {
		this.distanceSq = ( (this.source.x-this.dest.x) * (this.source.x-this.dest.x) ) + ( (this.source.y-this.dest.y) * (this.source.y-this.dest.y) ) * distFactor;
		this.softness = 1;
	} else {
		this.distanceSq = softDist * softDist;
		this.softness = 0.1;
	}

	this.iterate = function() {
		var delta = {x : (this.source.x - this.dest.x), y : (this.source.y - this.dest.y)};
		var correction = (( this.distanceSq/((delta.x*delta.x) + (delta.y*delta.y) + this.distanceSq) ) - 0.5) * this.softness;
		delta.x *= correction;
		delta.y *= correction;
		
		this.source.correctConstrains(delta, this.dest, 1);
		this.dest.correctConstrains(delta, this.source, -1);
	}

	this.getLengthSquared = function() {
		var y = this.source.y-this.dest.y;
		var x = this.source.x-this.dest.x;
		return (x*x) + (y*y);
	}

	this.getLength = function() {
		return Math.sqrt(this.getLengthSquared());
	}
	
	this.getAngle = function() {
		return Math.atan2(this.source.y - this.dest.y, this.source.x - this.dest.x);
	}
}

exports.Constrain = Constrain;
