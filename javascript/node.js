
function Node(x, y, fixed) {
	this.x = x;
	this.y = y;
	this.prevX = x;
	this.prevY = y;
	this.fixed = fixed;
	var drag = 0.9;

	this.iterate = function() {
		if (this.fixed) {
			return;
		}
		
		var tx = this.x;
		var ty = this.y;
		
		this.x += this.drag * (this.x - this.prevX);
		this.y += this.drag * (this.y - this.prevY);
		
		this.prevX = tx;
		this.prevY = ty;
	}

	this.correctConstrains = function(delta, sibling, dir) {
		if (!this.fixed) {
			this.x += delta.x*dir;
			this.y += delta.y*dir;
		}
	}
}

exports.Node = Node;

