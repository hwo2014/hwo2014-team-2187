var node = require('./node.js');
var constrain = require('./constrain.js');



function LaneBlock(piece, x, y, a) {
	this.startX = x;
	this.startY = y;
	this.startA = a;
	this.switching = (piece.switch != undefined && piece.switch == true);
	this.radius = 0;
	
	if (piece.radius == undefined) {
		this.endX = Math.cos(this.startA) * piece.length +  this.startX;
		this.endY = Math.sin(this.startA) * piece.length + this.startY;
		this.endA = this.startA;
	} else {
		this.radius = piece.radius;
		var angle = piece.angle * Math.PI/180;
		this.endA = this.startA + angle;
		
		var chord = 2 * this.radius * Math.sin(Math.abs(angle)/2);
		
		var beta = (Math.PI - angle) / 2;
		var alpha = -(Math.PI/2 - beta);
		
		var vx = chord * Math.cos(this.startA-alpha);
		var vy = chord * Math.sin(this.startA-alpha);
		
		this.endX = this.startX + vx;
		this.endY = this.startY + vy;
	}
	
	this.getLength = function(laneOffset) {
		if (piece.radius == undefined) {
			return piece.length;
		} else {
			if (piece.angle > 0)
				return (this.radius-laneOffset)*(Math.abs(this.endA-this.startA));
			else
				return (this.radius+laneOffset)*(Math.abs(this.endA-this.startA));
		}
	}
	
	this.getDifficultyFactor = function(laneOffset) {
		if (piece.radius == undefined) {
			return 0;
		} else {
			if (piece.angle > 0)
				return this.radius-laneOffset;
			else
				return this.radius+laneOffset;
		}
	} 
	
	this.node = new node.Node(this.endX, this.endY, false);
	this.nodeFixed = new node.Node(this.endX, this.endY, true);
	this.softLink = function(distance) {
		this.link = new constrain.Constrain(this.node, this.nodeFixed, 1, distance);
		return this.link;
	}
}

function Lane(pieces, lanes) {
	var maxLaneDistance = 0;
	for(key in lanes) {
		if (lanes[key].distanceFromCenter > maxLaneDistance)
			maxLaneDistance = lanes[key].distanceFromCenter;
	}
	console.log(lanes);
	
	var blocks = [];
	var constrains = [];
	var lastBlock = null;
	var block;
	//console.log(pieces)
	for(var i = 0; i < pieces.length; i++) {
		if (lastBlock == null)
			block = new LaneBlock(pieces[i], 0, 0, 0);
		else
			block = new LaneBlock(pieces[i], lastBlock.endX, lastBlock.endY, lastBlock.endA);
			
		
		if (lastBlock != null) {
			constrains.push(block.softLink(maxLaneDistance));
			constrains.push(new constrain.Constrain(lastBlock.node, block.node, 0.8, 0));
		}
	
		lastBlock = block;
		blocks.push(block);
	}
	//link full circuit
	block = blocks[0];
	constrains.push(block.softLink(maxLaneDistance));
	constrains.push(new constrain.Constrain(lastBlock.node, block.node, 0.8, 0));
	
	//simulate physics
	for(var i = 0; i < 10; i++) {
		for(var key in constrains)
			constrains[key].iterate();
	}

	this.printAngles = function() {
		for (var key in blocks) {	
			console.log("angles ", blocks[key].carAngle);
		}
	}
	
	this.canSwitch = function(index) {
		return blocks[index % (blocks.length-1)].switching;
	}
	
	this.getForce = function(index, factor) {
		var prev = blocks[(index-1+blocks.length) % (blocks.length-1)].link.getLengthSquared();
		var next = blocks[(index) % (blocks.length-1)].link.getLengthSquared();
		return (next-prev)*factor + prev;
	}


	
	
	


	this.getSmartThrottle = function(car) {
		//car position 0-1
		var position = car.posPositive / blocks[car.pieceIndex].getLength(lanes[car.laneIndex].distanceFromCenter);
		var velocity = car.velocity;
		/*var prevR = blocks[car.pieceIndex].radius;
		var nextR = blocks[(car.pieceIndex+1) % (blocks.length-1)].radius;

		var prevSideForce = (prevR > 0) ? velocity*velocity / prevR : 0; //assumption
		var nextSideForce = (nextR > 0) ? velocity*velocity / nextR : 0; //if velocity is constant
		var currentSideForce = prevSideForce + ((nextSideForce-prevSideForce)*position);
		*/
		//get
		function getForceArea(x, index) {
			var prevR = blocks[(index) % (blocks.length-1)].radius;
			var nextR = blocks[(index+1) % (blocks.length-1)].radius;
			
			var prevSideForce = (prevR > 0) ? 1 / prevR : 0; 
			var nextSideForce = (nextR > 0) ? 1 / nextR : 0; 
			return prevSideForce + ((nextSideForce-prevSideForce)*x);
			
		}
		
		//acceleration length
		var deLen = velocity*velocity*2;
		var force = 0;
		var index = car.pieceIndex;
		var currentLen = blocks[index].getLength(lanes[car.laneIndex].distanceFromCenter);
		var len = (1-position)*currentLen;
		
		while (deLen > 0) {
			if (deLen > len) {
				deLen = deLen - len;
				var x = (deLen + (position*currentLen)) / currentLen;
				force = getForceArea(x, index) - getForceArea(position, index);
				position = 0;
			} else {
				var x = (deLen + (position*currentLen)) / currentLen;
				force = getForceArea(x, index) ;
				deLen -= len;
			}
			index++;
			currentLen = blocks[(index) % (blocks.length-1)].getLength(0);
			len = (1-position)*currentLen;
		}
		force *= velocity*velocity*1.9;
		console.log('side force', force, velocity);
		
		return Math.max(1-force, 0);
	}
	
	

	
	this.getDifficultyFactor2 = function(car) {
		var sum = 0;
		var factor = 1;
		var segments = 3;
		var laneDist = lanes[car.laneIndex].distanceFromCenter;
		for (var i = 0; i < segments; i++) {
			var ind = (car.pieceIndex + i) % (blocks.length-1);
			var val = blocks[ind].getDifficultyFactor(laneDist);
			sum += val*factor;
			//factor -= 1/(segments*segments);
			//factor -= i/(segments);
		}
		
		var straightCount = 0;
		for (var i = 1; i <= blocks.length; i++) {
			var ind = (car.pieceIndex + blocks.length - i) % (blocks.length-1);
			var val = blocks[ind].getDifficultyFactor(laneDist);
			if (val == 0)
				straightCount++;
			else
				break;
		}
		
		//if (blocks[car.pieceIndex].gravity != 0)
		//	sum = sum*car.velocity*car.velocity/blocks[car.pieceIndex].gravity;
		//else
			//sum = sum*car.velocity*car.velocity/(110-(straightCount*3));
		sum = sum*car.velocity*car.velocity*car.velocity/630;
		console.log("diff fac", sum, straightCount);
		if(Math.abs(car.angle) > 45) {
			return 0;
		}
		return Math.max(Math.min(1-sum, 1), 0);
		
		
		
		var angleFactor = Math.abs(car.angle)-Math.abs(car.anglePrev);
		if(angleFactor > 0 && Math.abs(car.angle) > 30) {
			//console.log("angle ",angleFactor)
			sum = sum*(1+angleFactor);
			//sum = sum*((Math.abs(car.angle)-20)/5)
			//return 1;
		} else if(angleFactor < 0) {
			//console.log("angle ",angleFactor)
			
			sum *= 0.5;
		}
		
		var newThrottle = Math.max(Math.min(1-sum, 1), 0);
		if (newThrottle > car.throttle)
			return Math.min(newThrottle*1.1, 1);
		
		return newThrottle;
	}
	
	this.getBestLane = function(index, laneIndex) {
		var sum = 0;
		var i = index;
		var found = false;
		var count = 0;
		//find next switching point
		while (!found) {
			i++;
			i = i % (blocks.length-1);
			
			var angle = blocks[i].link.getAngle() - blocks[i].endA;
			angle = angle % (Math.PI*2);
			while (angle < 0)
				angle += Math.PI*2;
			
			//console.log(angle);
			var factor = (angle < Math.PI) ? -1 : 1;
			var len = blocks[i].link.getLengthSquared(); 
			//console.log(len, factor);
			sum += blocks[i].link.getLengthSquared() * factor;
			found = blocks[i].switching; 
			count++;
		}
		console.log("segment value", sum/count)
		
		//TODO lane for more than 2 lines
		if (sum > 0) { 
			return Math.max(laneIndex - 1, 0);
		} else {
			return Math.min(laneIndex + 1, lanes.length-1);
		}
	}
	
	var debug = false;  
	if (debug) {
		var path = '<svg width="1000" height="2000">';
		path += '<path fill="none" stroke="red" stroke-width="3" d="';
		for(var i = 0; i < blocks.length; i++) {
			var b = blocks[i];
			if (i == 0) 
				path += 'M';
			else
				path += ' L';
			path += (b.startX+600) + ' ' + (b.startY+300);
			console.log(b.startX, b.startY)
		}
		path += ' Z" /></svg>';
		//console.log(path)
		
		var fs = require('fs');
		var stream = fs.createWriteStream('test.html');
		stream.write(path);
		stream.on("end", function() {
		  stream.end();
		});
	}
}

exports.Lane = Lane;
