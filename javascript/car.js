
function Car(data) {
	this.name = data.name;
	this.color = data.color;
	
	this.pieceIndex = 0;
	this.pieceIndexPrev = -1;
	this.laneIndex = 0;
	
	this.posPositive = 0;
	
	this.throttle = 0;
	this.angle = 0;
	this.anglePrev = 0;
	this.throttle = 1.0;
	this.velocity = 0;
	
	this.feedback = 90;
	
	this.progress = function(carPositions, lane) {
		this.lane = lane;
		var result;
		var myData;
		
		for(var key in carPositions) {
			if (carPositions[key].id.color === this.color) {
				myData = carPositions[key];
				break;
			}
		}
		
		this.anglePrev = this.angle;
		this.angle = myData.angle;
		
		
		this.pieceIndex = myData.piecePosition.pieceIndex;
		var posPositive = myData.piecePosition.inPieceDistance;
		if (this.pieceIndex != this.pieceIndexPrev) {
			if (!this.crashed) {
				this.feedback += 5;
			}
		} else {
			this.velocity = posPositive - this.posPositive;
		}
		
		this.laneIndex = myData.piecePosition.lane.startLaneIndex;
		this.posPositive = posPositive;
		
		this.throttle = lane.getSmartThrottle(this);
		
		
		if (this.pieceIndex != this.pieceIndexPrev && lane.canSwitch(this.pieceIndex+1)) {
			this.laneIndex = myData.piecePosition.lane.startLaneIndex;
			console.log("Switching lane ", this.pieceIndex);
			var bestLane = lane.getBestLane(this.pieceIndex+1, this.laneIndex);

			if (this.laneIndex != bestLane) {
				if (this.laneIndex < bestLane) {
					result = { msgType: "switchLane", data: "Right" };
				} else {
					result = { msgType: "switchLane", data: "Left" };
				}
			} else {
				result = { msgType: "throttle", data: this.throttle };
			}
		} else {
			result = { msgType: "throttle", data: this.throttle };
		}
		this.pieceIndexPrev = this.pieceIndex;
		return result;
	}
	

	this.crashed = false;
	this.crash = function(data) {
		if (this.color === data.color) {
			this.feedback -= 15;

			this.crashed = true;
		}
	}
}

exports.Car = Car;